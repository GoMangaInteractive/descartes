﻿/*
    Descartes AI - Genetic Algorithm to find the explicit equation of a list of point

    Copyright (C) 2017 Francesco Pio Squillante

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Descartes
{
    class Program
    {
        static void Main(string[] args)
        {
            int nPoints;
            List<Point> points = new List<Point>();
            int startPopulation, iterations, generations;

            Console.WriteLine("Descartes AI" + Environment.NewLine);
            Console.WriteLine("Written by Francesco Pio Squillante for educational purpouse. Distribuited under GNU/GPL license.");

            Console.WriteLine("How many points you have?");
            nPoints = int.Parse(Console.ReadLine());

            for (int i = 0; i < nPoints; i++)
            {
                Point temp;
                Console.WriteLine("Point " + i + ":" + Environment.NewLine);
                Console.Write("x = ");
                temp.x = double.Parse(Console.ReadLine());
                Console.Write("y = ");
                temp.y = double.Parse(Console.ReadLine());
                Console.WriteLine();
                points.Add(temp);
            }

            Console.WriteLine("What is the start population?");
            startPopulation = int.Parse(Console.ReadLine());

            Console.WriteLine("How many iteration for each generations?");
            iterations = int.Parse(Console.ReadLine());

            Console.WriteLine("What is the maximum number of generations?");
            generations = int.Parse(Console.ReadLine());
            
            Individual best = Genetic.Compute(points, startPopulation, iterations, generations, false);

            Console.Clear();

            Console.WriteLine("Descartes AI" + Environment.NewLine);
            Console.WriteLine("Best individual after computation: " + best.FirstGene.Build());
            Console.WriteLine("Best individual fitness: " + best.Fitness);
            Console.ReadLine();
        }
    }

}
