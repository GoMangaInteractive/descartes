﻿/*
    Descartes AI - Genetic Algorithm to find the explicit equation of a list of point

    Copyright (C) 2017 Francesco Pio Squillante

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Descartes
{
    /// <summary>
    /// Point class
    /// </summary>
    public struct Point
    {
        public double x;
        public double y;
    };

    /// <summary>
    /// Individual class extends IComparable (comparation is based on fitness)
    /// </summary>
    public class Individual : IComparable<Individual>
    {
        List<Point> referencePoints;
        TreeElement tree;
        double fitness;
        double maxFitness = 1000;

        /// <summary>
        /// List of reference points
        /// </summary>
        public List<Point> ReferencePoints { get { return referencePoints; } set { referencePoints = value; } }

        /// <summary>
        /// Head of the tree
        /// </summary>
        public TreeElement FirstGene { get { return tree; } }

        /// <summary>
        /// Fitness of the individual
        /// </summary>
        public double Fitness { get { return fitness; } }

        /// <summary>
        /// Maximum fitness of the population
        /// </summary>
        public double MaximumFitness { get { return maxFitness; } set { maxFitness = value; } }

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="refPoints">List of reference points</param>
        /// <param name="rnd">Random class</param>
        public Individual(List<Point> refPoints, Random rnd)
        {
            referencePoints = refPoints;
            tree = new TreeElement(TreeElement.TokenType.Operator, Tree.operators[rnd.Next(0, Tree.operators.Length - 1)], Tree.MakeRandomTree(rnd), Tree.MakeRandomTree(rnd));
            CalculateFitness();
        }

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="refPoints">List of reference points</param>
        /// <param name="rnd">Random class</param>
        /// <param name="op">Operator of the head of the tree</param>
        /// <param name="next1">Left branch of the head of the tree</param>
        /// <param name="next2">Right branch of the head of the tree</param>
        public Individual(List<Point> refPoints, Random rnd, string op, TreeElement next1, TreeElement next2)
        {
            referencePoints = refPoints;
            tree = new TreeElement(TreeElement.TokenType.Operator, op, Tree.MakeRandomTree(rnd), Tree.MakeRandomTree(rnd));
            CalculateFitness();
        }

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="refPoints">List of reference points</param>
        /// <param name="firstElement">Head of the tree</param>
        public Individual(List<Point> refPoints,TreeElement firstElement)
        {
            referencePoints = refPoints;
            tree = firstElement;
            CalculateFitness();
        }

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="refPoints">List of reference points</param>
        /// <param name="firstElement">Head of the tree</param>
        /// <param name="maxFitness">Maximum fitness in the population</param>
        public Individual(List<Point> refPoints, TreeElement firstElement, double maxFitness)
        {
            referencePoints = refPoints;
            tree = firstElement;
            this.maxFitness = maxFitness;
            CalculateFitness();
        }

        /// <summary>
        /// Calculates the fitness of the class according to reference points
        /// </summary>
        public void CalculateFitness()
        {
            double delta = 0;

            foreach(var point in referencePoints)
                delta += Math.Abs(point.y - tree.Evaluate(point.x));

            if (delta == 0)
                fitness = maxFitness + 1;
            else if (double.IsInfinity(delta))
                fitness = 0;
            else
                fitness = (double)1 / (delta / referencePoints.Count);
        }

        /// <summary>
        /// IComparable interface implementation
        /// </summary>
        /// <param name="other">Other Individual</param>
        /// <returns>Witch one is the greater</returns>
        public int CompareTo(Individual other)
        {
            return (-1) * fitness.CompareTo(other.Fitness);
        }
    }
}
