﻿/*
    Descartes AI - Genetic Algorithm to find the explicit equation of a list of point

    Copyright (C) 2017 Francesco Pio Squillante

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Descartes
{
    /// <summary>
    /// Tree node class
    /// </summary>
    public class TreeElement
    {
        /// <summary>
        /// Types of nodes
        /// </summary>
        public enum TokenType
        {
            UnaryFunction,
            Operator,
            Number,
            Variable
        };

        TokenType type;
        TreeElement next1;
        TreeElement next2;
        string value;

        /// <summary>
        /// Type of the node
        /// </summary>
        public TokenType Type { get { return type; } set { type = value; } }

        /// <summary>
        /// Left branch of the tree node
        /// </summary>
        public TreeElement Next1 { get { return next1; } set{ next1 = value; } }

        /// <summary>
        /// Right branch of the tree node
        /// </summary>
        public TreeElement Next2 { get { return next2; } set { next2 = value; } }

        /// <summary>
        /// Value of the node
        /// </summary>
        public string Value { get { return value; } }

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="type">Type of the node</param>
        /// <param name="value">Value of the node</param>
        /// <param name="next1">Left branch of the tree node</param>
        /// <param name="next2">Right branch of the tree node</param>
        public TreeElement(TokenType type, string value, TreeElement next1, TreeElement next2)
        {
            this.type = type;
            this.next1 = next1;
            this.next2 = next2;
            this.value = value;
        }

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="type">Type of the node</param>
        /// <param name="value">Value of the node</param>
        /// <param name="next">Branch of the tree node (assumed to be the left one)</param>
        public TreeElement(TokenType type, string value, TreeElement next)
        {
            this.type = type;
            this.next1 = next;
            this.value = value;
        }

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="type">Type of the node</param>
        /// <param name="value">Value of the node</param>
        public TreeElement(TokenType type, string value)
        {
            this.type = type;
            this.value = value;
        }

        /// <summary>
        /// Build a string that represents the tree (recursive)
        /// </summary>
        /// <returns>Function string</returns>
        public string Build()
        {
            switch (type)
            {
                case TokenType.Operator:
                    return next1.Build() + value + next2.Build();
                case TokenType.Number:
                    return value;
                case TokenType.UnaryFunction:
                    return value + "(" + next1.Build() + ")";
                case TokenType.Variable:
                    return value;
                default:
                    return "";
            }
        }

        /// <summary>
        /// Evaluate the tree (recursive)
        /// </summary>
        /// <param name="x">Variable value</param>
        /// <returns>The value of f(x)</returns>
        public double Evaluate(double x)
        {
            switch (type)
            {
                case TokenType.Operator:
                    switch (value)
                    {
                        case "+":
                            return next1.Evaluate(x) + next2.Evaluate(x);
                        case "-":
                            return next1.Evaluate(x) - next2.Evaluate(x);
                        case "*":
                            return next1.Evaluate(x) * next2.Evaluate(x);
                        case "/":
                            return next1.Evaluate(x) / next2.Evaluate(x);
                        case "^":
                            return Math.Pow(next1.Evaluate(x), next2.Evaluate(x));
                        case "%":
                            return next1.Evaluate(x) % next2.Evaluate(x);
                        default:
                            return double.NaN;
                    }
                case TokenType.Number:
                    return double.Parse(value);
                case TokenType.UnaryFunction:
                    switch (value)
                    {
                        case "sin":
                            return Math.Sin(next1.Evaluate(x));
                        case "cos":
                            return Math.Cos(next1.Evaluate(x));
                        case "tan":
                            return Math.Tan(next1.Evaluate(x));
                        case "asin":
                            return  Math.Asin(next1.Evaluate(x));
                        case "acos":
                            return Math.Acos(next1.Evaluate(x));
                        case "atan":
                            return Math.Atan(next1.Evaluate(x));
                        case "ln":
                            return Math.Log(next1.Evaluate(x));
                        case "log10":
                            return Math.Log10(next1.Evaluate(x));
                        case "exp":
                            return Math.Exp(next1.Evaluate(x));
                        case "sqrt":
                            return Math.Sqrt(next1.Evaluate(x));
                        case "sinh":
                            return Math.Sinh(next1.Evaluate(x));
                        case "cosh":
                            return Math.Cosh(next1.Evaluate(x));
                        case "tanh":
                            return Math.Tanh(next1.Evaluate(x));
                        case "abs":
                            return Math.Abs(next1.Evaluate(x));
                        default:
                            return double.NaN;
                    }
                case TokenType.Variable:
                    switch (value)
                    {
                        case "x":
                            return x;
                        case "x^2":
                            return Math.Pow(x, 2);
                        case "x^3":
                            return Math.Pow(x, 3);
                        case "x^4":
                            return Math.Pow(x, 4);
                        case "x^5":
                            return Math.Pow(x, 5);
                        default:
                            return double.NaN;
                    }
                default:
                    return double.NaN;
            }
        }
    }

    /// <summary>
    /// Tree utilities class
    /// </summary>
    public static class Tree
    {
        /// <summary>
        /// Array of available functions tokens
        /// </summary>
        public static string[] unaryFunctions = { "sin", "cos", "tan", "asin", "acos", "atan", "ln", "log10", "exp", "sqrt", "sinh", "cosh", "tanh", "abs"};

        /// <summary>
        /// Array of available operators tokens
        /// </summary>
        public static string[] operators = { "+", "-", "*", "/", "^", "%" };

        /// <summary>
        /// Array of available variables tokens
        /// </summary>
        public static string[] variables = { "x", "x^2", "x^3", "x^4", "x^5" };

        /// <summary>
        /// Generates a random double in the interval ]-3;3[
        /// </summary>
        /// <param name="rnd">Random class</param>
        /// <returns>A random double value between -3 and 3</returns>
        static double NextDouble(Random rnd)
        {
            return rnd.NextDouble() * 6 - 3;
        }

        /// <summary>
        /// Generates a random tree
        /// </summary>
        /// <param name="rnd">Random class</param>
        /// <returns>A random generated tree branch</returns>
        public static TreeElement MakeRandomTree(Random rnd)
        {
            switch (rnd.Next(1, 6))
            {
                case 1:
                    return new TreeElement(TreeElement.TokenType.UnaryFunction, unaryFunctions[rnd.Next(0, unaryFunctions.Length)], MakeRandomTree(rnd));
                case 2:
                    return new TreeElement(TreeElement.TokenType.Operator, operators[rnd.Next(0, operators.Length)], MakeRandomTree(rnd), MakeRandomTree(rnd));
                case 3:
                    return new TreeElement(TreeElement.TokenType.Variable, variables[rnd.Next(0, variables.Length)]);
                default:
                    return new TreeElement(TreeElement.TokenType.Number, NextDouble(rnd).ToString("f", System.Globalization.CultureInfo.InvariantCulture));
            }
        }

    }
}
