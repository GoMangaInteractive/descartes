﻿/*
    Descartes AI - Genetic Algorithm to find the explicit equation of a list of point

    Copyright (C) 2017 Francesco Pio Squillante

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Descartes
{
    /// <summary>
    /// Class for crossing over
    /// </summary>
    public static class Crossover
    {
        /// <summary>
        /// Structure to weigh random crossover
        /// </summary>
        struct RandomSelection
        {
            int index;
            public double probability;

            public RandomSelection(int index, double probability)
            {
                this.index = index;
                this.probability = probability;
            }

            public int GetValue() { return index; }
        }

        /// <summary>
        /// Get a random index basing on best fitness
        /// </summary>
        /// <param name="rnd">Random class</param>
        /// <param name="selections">Weights of each individual</param>
        /// <returns>Index of a random individual basing on its fitness</returns>
        static int GetRandomValue(Random rnd, params RandomSelection[] selections)
        {
            double rand = rnd.NextDouble();
            double currentProb = 0;
            foreach (var selection in selections)
            {
                currentProb += selection.probability;
                if (rand <= currentProb)
                    return selection.GetValue();
            }

            return -1;
        }

        /// <summary>
        /// Evolve population (extinguish half population basing on individuals fitness)
        /// </summary>
        /// <param name="individuals">Population list</param>
        public static void NextGeneration(ref List<Individual> individuals)
        {
            individuals.Sort();
            individuals.RemoveRange(individuals.Count / 2, individuals.Count / 2);
        }

        /// <summary>
        /// Do crossover
        /// </summary>
        /// <param name="individuals">Population list</param>
        /// <param name="rnd">Random class</param>
        public static void Evolve(ref List<Individual> individuals, Random rnd)
        {
            double totalFitness = 0;
            RandomSelection[] randoms = new RandomSelection[individuals.Count];

            double maxFit = individuals.Max().Fitness;

            foreach (var individual in individuals)
            {
                if (!double.IsNaN(individual.Fitness))
                    totalFitness += individual.Fitness;

                individual.MaximumFitness = maxFit;
            }

            if (double.IsInfinity(totalFitness))
                totalFitness = double.MaxValue;

            for (int i = 0; i < individuals.Count; i++)
                if (double.IsNaN(individuals[i].Fitness))
                    randoms[i] = new RandomSelection(i, 0);
                else if (double.IsInfinity(individuals[i].Fitness))
                    randoms[i] = new RandomSelection(i, 1);
                else
                    randoms[i] = new RandomSelection(i, (double)individuals[i].Fitness / totalFitness);

            int rndIndex1 = GetRandomValue(rnd, randoms);
            int rndIndex2 = GetRandomValue(rnd, randoms);

            TreeElement temp1 = individuals[rndIndex1].FirstGene.Next1;
            TreeElement temp2 = individuals[rndIndex1].FirstGene.Next2;
            TreeElement temp3 = individuals[rndIndex2].FirstGene.Next1;
            TreeElement temp4 = individuals[rndIndex2].FirstGene.Next2;

            individuals.Add(new Individual(individuals[rndIndex1].ReferencePoints, rnd, individuals[rndIndex1].FirstGene.Value, temp2, temp1));
            individuals.Add(new Individual(individuals[rndIndex1].ReferencePoints, rnd, individuals[rndIndex1].FirstGene.Value, temp1, temp3));
            individuals.Add(new Individual(individuals[rndIndex1].ReferencePoints, rnd, individuals[rndIndex1].FirstGene.Value, temp2, temp4));
            individuals.Add(new Individual(individuals[rndIndex1].ReferencePoints, rnd, individuals[rndIndex1].FirstGene.Value, temp4, temp2));
            individuals.Add(new Individual(individuals[rndIndex1].ReferencePoints, rnd, individuals[rndIndex1].FirstGene.Value, temp3, temp1));

            individuals.Add(new Individual(individuals[rndIndex1].ReferencePoints, rnd, individuals[rndIndex2].FirstGene.Value, temp4, temp3));
            individuals.Add(new Individual(individuals[rndIndex1].ReferencePoints, rnd, individuals[rndIndex2].FirstGene.Value, temp1, temp3));
            individuals.Add(new Individual(individuals[rndIndex1].ReferencePoints, rnd, individuals[rndIndex2].FirstGene.Value, temp2, temp4));
            individuals.Add(new Individual(individuals[rndIndex1].ReferencePoints, rnd, individuals[rndIndex2].FirstGene.Value, temp4, temp2));
            individuals.Add(new Individual(individuals[rndIndex1].ReferencePoints, rnd, individuals[rndIndex2].FirstGene.Value, temp3, temp1));

        }
    }
}
