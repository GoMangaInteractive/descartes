﻿/*
    Descartes AI - Genetic Algorithm to find the explicit equation of a list of point

    Copyright (C) 2017 Francesco Pio Squillante

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Descartes
{
    /// <summary>
    /// A class that represents a possibily infinitely looping load screen.
    /// </summary>
    public class ConsoleLoadingText
    {
        static string[] spinner = { ".  ", " . ", "  .", " . " };
        string loadingValue = "";

        string loadingText;
        int millisecondsDelay;

        /// <summary>
        /// Text shown during loading
        /// </summary>
        public string LoadingText { get { return loadingText; } }

        /// <summary>
        /// Delay for animation
        /// </summary>
        public int MillisecondsDelay { get { return millisecondsDelay; } }

        public string LoadingValue { get { return loadingValue; } set { loadingValue = value; } }

        int i;
        bool _continue = true;

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="text">Text shown during loading</param>
        /// <param name="delay">Delay for animation (milliseconds)</param>
        public ConsoleLoadingText(string text, int delay)
        {
            loadingText = text;
            millisecondsDelay = delay;
        }

        /// <summary>
        /// Starts the instance for displaying.
        /// </summary>
        public async void Display()
        {
            await Task.Run(() =>
            {
                _continue = true;
                while (_continue)
                {
                    Console.Write($"\r{loadingText}{spinner[i]} {loadingValue}  ");
                    i = (i + 1) % spinner.Length;
                    System.Threading.Thread.Sleep(millisecondsDelay);
                }
            });
        }

        /// <summary>
        /// Stops this instance from displaying.
        /// </summary>
        public void Stop()
        {
            _continue = false;
        }
    }

    public static class Genetic
    {
        /// <summary>
        /// Print a verbose output of the population
        /// </summary>
        /// <param name="population">Population to show</param>
        public static void PrintPopulation(List<Individual> population)
        {
            Console.Clear();
            Console.WriteLine("Descartes AI" + Environment.NewLine);
            foreach (var individual in population)
                Console.WriteLine(individual.FirstGene.Build() + " Fitness: " + individual.Fitness);
        }

        /// <summary>
        /// Starts computation
        /// </summary>
        /// <param name="points">Points for reference</param>
        /// <param name="StartPopulation">Start population individuals count</param>
        /// <param name="iterationsPerGeneration">Crossing overs for each generation</param>
        /// <param name="generations">Number of max generations</param>
        /// <param name="verbose">Show verbose output?</param>
        /// <returns>The best individual after computation</returns>
        public static Individual Compute(List<Point> points, int StartPopulation, int iterationsPerGeneration, int generations, bool verbose)
        {
            List<Individual> population = new List<Individual>();
            Random rnd = new Random((int)DateTime.Now.Ticks);
            ConsoleLoadingText spinner = new ConsoleLoadingText("Descartes is thinking", 250);

            if (!verbose)
            {
                Console.Clear();
                Console.CursorVisible = false;
                spinner.Display();
            }

            for (int i = 0; i < StartPopulation; i++)
                population.Add(new Individual(points, rnd));
            
            for (int p = 0; p < generations; p++)
            {
                spinner.LoadingValue = (((double)p / generations) * 100).ToString(System.Globalization.CultureInfo.InvariantCulture) + "%";
                for (int i = 0; i < iterationsPerGeneration; i++)
                {
                    if (verbose)
                        PrintPopulation(population);
                    Crossover.Evolve(ref population, rnd);
                }
                Crossover.NextGeneration(ref population);
            }

            spinner.Stop();
            return population[0];
        }
    }
}
